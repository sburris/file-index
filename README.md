# About
This is a user interface made for a fileserver using the Nginx [fancyindex module](https://github.com/aperezdc/ngx-fancyindex) 

This repository includes the HTML and CSS for the frontend, this project does not use JavaScript. 
No Nginx configurations are included.

## Screenshots
![Screenshot](screenshots/index_view.png)
![Screenshot](screenshots/directory_view.png)
